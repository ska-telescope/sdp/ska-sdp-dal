import time
import sys

from ska_sdp_dal.examples.stream import StreamProcessorSimple

if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("Please provide plasma socket path!")
        print("  Example: python stream_processor.py /tmp/plasma")
        exit(1)

    # Create processor, process calls forever
    report_rate = 1
    proc = StreamProcessorSimple(sys.argv[1])
    checkpoint = time.time() + report_rate
    while True:
        try:
            while time.time() >= checkpoint:
                print(proc.bytes_received / report_rate / 1e9, "GB/s")
                proc.bytes_received = 0
                checkpoint += report_rate
            proc.process(max(0, checkpoint - time.time()))
        except KeyboardInterrupt:
            exit(0)
