import numpy
import pyarrow

import ska_sdp_dal as rpc

STREAM_PROCS = [
    rpc.make_call_schema(
        "stream",
        [
            rpc.make_tensor_input_par("input", pyarrow.float64(), ["x"]),
            rpc.make_tensor_output_par("tag", pyarrow.float64(), []),
        ],
    )
]


class StreamCaller(rpc.Caller):
    def __init__(self, *args, **kwargs):
        super(StreamCaller, self).__init__(STREAM_PROCS, *args, **kwargs)


class StreamProcessor(rpc.BasicProcessor):
    def __init__(self, *args, **kwargs):
        super(StreamProcessor, self).__init__(STREAM_PROCS, *args, **kwargs)
        self.bytes_received = 0

    def _process_call(self, proc_name, batch):

        if proc_name != "stream":
            raise ValueError("Call to unknown procedure {}!".format(proc_name))

        # Get input
        inp = self.tensor_parameter(batch, "input", pyarrow.float64())

        # Log number of bytes received
        self.bytes_received += memoryview(inp).nbytes

        # Tag done
        self.output_tensor(batch, "tag", numpy.empty(0))


class StreamProcessorSimple(rpc.Processor):
    def __init__(self, *args, **kwargs):
        super(StreamProcessorSimple, self).__init__(
            STREAM_PROCS, *args, **kwargs
        )
        self.bytes_received = 0

    def stream(self, input, tag):

        # Log number of bytes received
        self.bytes_received += memoryview(input.get()).nbytes

        # Tag done
        tag.put()
