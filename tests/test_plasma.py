import gc

import numpy
from pyarrow import plasma

from ska_sdp_dal import common
from ska_sdp_dal.connection import Connection
from ska_sdp_dal.dump_cli import main
from ska_sdp_dal.examples.stream import (
    STREAM_PROCS,
    StreamCaller,
    StreamProcessor,
)
from ska_sdp_dal.store import Store

from .fixtures import get_plasma_store  # noqa: F401


def test_simple_namespace_drop(plasma_store):

    # Create store (which should register a namespace), get underlying
    # connection
    store = Store(plasma_store)
    conn = store.conn

    # There should be one namespace now
    conn.update_obj_table()
    assert len(conn.namespaces) == 1

    # Drop store - there should be no namespace now
    store = None
    conn.update_obj_table()
    assert len(conn.namespaces) == 0


def test_simple_caller_drop(plasma_store):

    # Create store (which should register a namespace), get underlying
    # connection
    store = Store(plasma_store)
    _caller = StreamCaller(store, broadcast=True, minimum_processors=0)
    conn = store.conn

    # There should be one namespace now
    conn.update_obj_table()
    assert len(conn.namespaces) == 1

    # Drop store - there should be no namespace now
    store = None
    conn.update_obj_table()
    assert len(conn.namespaces) == 1
    _caller = None  # noqa: F841
    gc.collect()
    conn.update_obj_table()
    assert len(conn.namespaces) == 0


def test_stream(plasma_store):

    # Create caller and processor
    proc = StreamProcessor(plasma_store)
    store = Store(plasma_store)
    caller = StreamCaller(store, broadcast=True)
    assert caller.find_processors() == 1

    # Stream
    packet = numpy.arange(1000, dtype=float)
    nbytes = memoryview(packet).nbytes
    PACKETS = 10
    for i in range(PACKETS):

        # Issue the call
        tag_ref = store.new_tensor_ref()
        caller.stream(packet, tag_ref)  # pylint: disable=no-member

        # Check that the call gets received eventually
        assert proc.bytes_received == i * nbytes
        while proc.bytes_received < (i + 1) * nbytes:
            proc.process(catch_exceptions=False)

        # Check that tag is now available
        tag_ref.get()

    # Check that we can add a second processor
    proc2 = StreamProcessor(plasma_store)
    assert caller.find_processors() == 2
    for i in range(PACKETS):

        # Call - and this time have caller generate refs automatically
        tag_refs = caller.stream(  # pylint: disable=no-member
            numpy.arange(1000, dtype=float)
        )
        assert len(tag_refs) == 2

        # Check that the call gets received eventually
        assert proc.bytes_received == (PACKETS + i) * nbytes
        assert proc2.bytes_received == i * nbytes
        while proc.bytes_received < (PACKETS + i + 1) * nbytes:
            proc.process(catch_exceptions=False)
        while proc2.bytes_received < (i + 1) * nbytes:
            proc2.process(catch_exceptions=False)

        # Tags should be available now
        for tag_ref in tag_refs:
            tag_ref["tag"].get()


def test_cli(plasma_store, capsys):

    # Plasma should be empty at start
    gc.collect()
    capsys.readouterr()
    main([plasma_store])
    captured = capsys.readouterr()
    print(captured.out)
    assert not captured.out

    # Generate something to look at in the store: Create caller and
    # processor, make a call
    proc = StreamProcessor(plasma_store)
    store = Store(plasma_store)
    caller = StreamCaller(store, broadcast=True)
    tag_refs = caller.stream(  # pylint: disable=no-member
        numpy.arange(1000, dtype=float)
    )

    # Dump verbose
    main([plasma_store])
    captured = capsys.readouterr()
    assert " Table proc:namespace=StreamProcessor" in captured.out
    assert " Table proc:func=stream" in captured.out
    assert " Table proc:namespace=Store" in captured.out
    assert "B double[1000]\n" in captured.out
    assert " -> double[1000]" in captured.out
    assert " -> not found" in captured.out
    expected = """
  * stream(
      input :fixed_size_binary[20] !null proc:tensor_type=float64 proc:tensor_dims=x proc:par=in,
      tag :fixed_size_binary[20] !null proc:tensor_type=float64 proc:tensor_dims= proc:par=out)"""  # noqa: E501
    assert expected in captured.out

    # Dump quiet (no lookup)
    main(["-q", plasma_store])
    captured = capsys.readouterr()
    assert " -> float64[1000]" not in captured.out
    assert " -> not found" not in captured.out

    # Dump extra-quiet (no type signatures)
    main(["-qq", plasma_store])
    assert "* stream(input, tag)" in capsys.readouterr().out

    # Test with evictions
    main(["--evict", plasma_store])

    # Now process
    while proc.bytes_received == 0:
        proc.process(catch_exceptions=False)

    # Check that tag is there, and vanishes once the reference goes
    # out of scope
    main([plasma_store])
    assert " double[0]" in capsys.readouterr().out
    tag_refs[0]["tag"].get()
    tag_refs = None
    main([plasma_store])
    assert " double[0]" not in capsys.readouterr().out


def test_connection(plasma_store):

    # Make a connection to the store, should have no namespaces at start
    gc.collect()
    conn = Connection(plasma_store)
    assert not conn.namespaces

    # Register a namespace, make sure it is registered correctly
    pf1, _root1 = conn.reserve_namespace("test", STREAM_PROCS)
    pf1_ns_oid = plasma.ObjectID(next(common.objectid_generator(pf1)))
    conn.update_obj_table(0.1)
    assert conn.namespaces == [pf1_ns_oid]
    for proc in STREAM_PROCS:
        assert conn.namespace_procs[pf1_ns_oid][common.call_name(proc)] == proc

    # Make second connection, make sure it has the same view
    conn2 = Connection(plasma_store)
    assert conn2.namespaces == conn.namespaces
    assert conn2.namespace_meta == conn.namespace_meta
    assert conn2.namespace_procs == conn.namespace_procs

    # Make third connection, make sure it has the same view
    conn2 = Connection(plasma_store)

    # Clear first namespace
    _root1 = None  # noqa: F841
    conn.update_obj_table(0.1)
    conn2.update_obj_table(0.1)
    assert not conn.namespaces
    assert not conn2.namespaces
