import numpy
import pyarrow
import pytest

from ska_sdp_dal import Caller, Processor, common
from ska_sdp_dal.store import Store

from .fixtures import get_plasma_store  # noqa: F401


class Except(Exception):
    pass


PROCS = [
    common.make_call_schema(
        "throw_exception",
        [
            common.make_tensor_input_par("in_tag", pyarrow.float64(), []),
            common.make_tensor_output_par("out_tag", pyarrow.float64(), []),
        ],
    ),
    common.make_call_schema(
        "intermediate",
        [
            common.make_tensor_input_par("in_tag", pyarrow.float64(), []),
            common.make_tensor_output_par("out_tag", pyarrow.float64(), []),
        ],
    ),
]


class Proc(Processor):
    def __init__(self, plasma_store):
        super(Proc, self).__init__(PROCS, plasma_store)
        self.received = 0

    def throw_exception(self, in_tag, out_tag):
        self.received += 1
        raise Except()

    def intermediate(self, in_tag, out_tag):
        out_tag.put()


def test_exception(plasma_store, caplog):

    # Instantiate
    proc = Proc(plasma_store)
    caller = Caller(PROCS, Store(plasma_store))
    assert caller.find_processors() == 1

    # pylint: disable=no-member
    def inter_tag():
        return caller.intermediate(numpy.empty(0))["out_tag"]

    for make_tag in [inter_tag, lambda: numpy.empty(0)]:
        for catch in [True, False]:

            # Call, make sure exception gets absorbed by default
            recvd = proc.received
            _tag_ref = caller.throw_exception(make_tag())  # noqa: F841

            if catch:
                caplog.clear()
                while proc.received == recvd:
                    proc.process(catch_exceptions=catch)
                # Should have appeared in the log
                assert len(caplog.records) == 1
                assert (
                    "While processing call" in caplog.records[0].getMessage()
                )
                assert "to throw_exception" in caplog.records[0].getMessage()
                assert caplog.records[0].exc_info[0] == Except
                caplog.clear()
            else:
                # Should raise right away
                with pytest.raises(Except):
                    while proc.received == recvd:
                        proc.process(catch_exceptions=False)
