import gc
import time

import awkward
import numpy
import pyarrow
import pytest

try:
    import pandas  # pylint: disable=import-error

    HAVE_PANDAS = True
except Exception:
    HAVE_PANDAS = False

from ska_sdp_dal import Caller, Processor, common
from ska_sdp_dal.connection import TensorRef
from ska_sdp_dal.store import Store

from .fixtures import get_plasma_store  # noqa: F401


def test_simple_processor_missing_handler():

    # Create processor class
    class Proc(Processor):
        def __init__(self):
            super(Proc, self).__init__(
                [common.make_call_schema("test", [])], ""
            )

    msg = r"Processor None \(Proc\) is missing handler for test!"
    with pytest.raises(TypeError, match=msg):
        Proc()


# Types to check
TENSOR_TYPES = [
    pyarrow.int8(),
    pyarrow.int16(),
    pyarrow.int32(),
    pyarrow.int64(),
    pyarrow.uint8(),
    pyarrow.uint16(),
    pyarrow.uint32(),
    pyarrow.uint64(),
    pyarrow.float16(),
    pyarrow.float32(),
    pyarrow.float64(),
]
TYPES = TENSOR_TYPES + [
    # pyarrow.bool_(), # Gets copied by Awkward!
    pyarrow.string()
]


def example_value(t, i=1):
    if t == pyarrow.bool_():
        return i % 2 == 0
    if t == pyarrow.float16():
        return numpy.float16(123 + i)
    if t == pyarrow.string():
        return "test" * i
    return t.id + i


# Derive procedure schemas
PROCS = []
for t in TYPES:
    PROCS.append(
        common.make_call_schema(
            "test_" + str(t),
            [
                common.make_par("inp", t, nullable=True),
                common.make_par("is_null", pyarrow.bool_()),
                common.make_tensor_output_par("tag", pyarrow.float64(), []),
            ],
        )
    )
    if t in TENSOR_TYPES:
        PROCS.append(
            common.make_call_schema(
                "test_" + str(t) + "_tensor",
                [
                    common.make_tensor_input_par(
                        "inp", t, ["x"], nullable=True
                    ),
                    common.make_par("is_null", pyarrow.bool_()),
                    common.make_tensor_output_par(
                        "tag", pyarrow.float64(), []
                    ),
                ],
            )
        )

PROCS.append(
    # Function that takes a parameter of each type
    common.make_call_schema(
        "test_all",
        [common.make_par("inp_" + str(t), t) for t in TYPES]
        + [common.make_tensor_output_par("tag", pyarrow.float64(), [])],
    )
)
PROCS.append(
    # Function that takes a reference to a table containing a field
    # for each type
    common.make_call_schema(
        "test_all_table",
        [
            common.make_table_input_par(
                "inp_table",
                pyarrow.schema(
                    [common.make_par("inp_" + str(t), t) for t in TYPES]
                ),
            ),
            common.make_tensor_output_par("tag", pyarrow.float64(), []),
        ],
    ),
)


class TypesProc(Processor):
    def __init__(self, plasma_store):

        for t in TYPES:

            def call_method(inp, tag, is_null, _typ=t):
                # Null?
                if is_null:
                    assert inp is None
                else:
                    # Check value
                    assert inp == example_value(_typ)
                    # Convert into pyarrow array and back to check precise type
                    arr = pyarrow.array([example_value(_typ)], type=_typ)
                    assert arr[0].as_py().__class__ == inp.__class__
                # Acknowledge
                self.received += 1
                tag.put()

            setattr(self, "test_" + str(t), call_method)

        for t in TENSOR_TYPES:

            def call_method_tensor(inp, tag, is_null, _typ=t):
                if is_null:
                    assert inp is None
                else:
                    assert isinstance(inp, TensorRef)
                    tensor = inp.get()
                    assert tensor.dtype == numpy.dtype(_typ.to_pandas_dtype())
                    assert tensor[0] == example_value(_typ)
                self.received += 1
                tag.put()

            setattr(self, "test_" + str(t) + "_tensor", call_method_tensor)

        super(TypesProc, self).__init__(PROCS, plasma_store)
        self.received = 0

    def test_all(self, tag, **args):
        for t in TYPES:
            assert args["inp_" + str(t)] == example_value(t)
        self.received += 1
        tag.put()

    def test_all_table(self, inp_table, tag):

        # Check we can read it raw
        table = inp_table.get()
        assert isinstance(table, pyarrow.Table)
        for t in TYPES:
            for i, v in enumerate(table["inp_" + str(t)]):
                assert v.as_py() == example_value(t, i)

        # ... and as Awkward array
        awk = inp_table.get_awkward()
        assert isinstance(awk, awkward.Array)
        # The string representation of the layout has the pointers
        # used for accessing the column data - we can use this to
        # assert that no buffers were copied, and therefore we are
        # using the
        assert str(awk.layout) == str(inp_table.get_awkward().layout)
        for t in TYPES:
            for i, v in enumerate(awk["inp_" + str(t)]):
                assert v == example_value(t, i)

        # ... as Pandas dataframe
        if HAVE_PANDAS:
            df = inp_table.get_pandas()
            assert isinstance(df, pandas.DataFrame)
            for t in TYPES:
                for i, v in enumerate(df["inp_" + str(t)]):
                    assert v == example_value(t, i)

        # ... and as Python dictionary
        dct = inp_table.get_dict()
        assert isinstance(dct, dict)
        for t in TYPES:
            for i, v in enumerate(dct["inp_" + str(t)]):
                assert v == example_value(t, i)

        self.received += 1
        tag.put()


def test_types(plasma_store):

    # Instantiate
    proc = TypesProc(plasma_store)
    caller = Caller(PROCS, Store(plasma_store), processor_prefix=proc.prefix)
    assert caller.find_processors() == 1

    for check_null in [False, True]:

        # Test the different calls
        received0 = proc.received
        for i, t in enumerate(TYPES):

            # Send call, check that it is received
            fn = getattr(caller, f"test_{t}")
            tag = fn(None if check_null else example_value(t), check_null)[
                "tag"
            ]
            assert proc.received == received0 + i
            while proc.received < received0 + i + 1:
                proc.process(catch_exceptions=False)
            tag.get()

        # Test the different tensor calls
        received0 = proc.received
        for i, t in enumerate(TENSOR_TYPES):

            # Again - send call, check that it is received
            tensor = pyarrow.Tensor.from_numpy(
                numpy.array(t.id * [example_value(t)])
            )
            fn = getattr(caller, f"test_{t}_tensor")
            tag = fn(None if check_null else tensor, check_null)["tag"]
            assert proc.received == received0 + i
            while proc.received < received0 + i + 1:
                proc.process(catch_exceptions=False)
            tag.get()

    # Ensure processor gets deleted before we continue
    proc = None
    gc.collect()


def test_types_all(plasma_store):

    time.sleep(1)

    # Again, instantiate
    proc = TypesProc(plasma_store)
    caller = Caller(PROCS, Store(plasma_store), processor_prefix=proc.prefix)
    assert caller.find_processors() == 1

    # Test passing all parameters at once
    received1 = proc.received
    input_values = {"inp_" + str(t): example_value(t) for t in TYPES}
    tag = caller.test_all(**input_values)["tag"]
    assert proc.received == received1
    while proc.received < received1 + 1:
        proc.process(catch_exceptions=False)
    tag.get()

    print(tag)

    # Test passing it as table
    for count in range(1, 10):
        input_values = {
            "inp_" + str(t): [example_value(t, i) for i in range(count)]
            for t in TYPES
        }
        tag = caller.test_all_table(input_values)["tag"]
        assert proc.received == received1 + count
        while proc.received < received1 + count + 1:
            proc.process(catch_exceptions=False)
        tag.get()

    # Ensure processor gets deleted before we continue
    proc = None
    gc.collect()
