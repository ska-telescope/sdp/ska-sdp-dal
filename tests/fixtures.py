import os
import subprocess
import tempfile

import pytest


@pytest.fixture(name="plasma_store", scope="session")
def get_plasma_store():
    with tempfile.TemporaryDirectory("sdp-dal-test") as plasma_dir:
        plasma_socket = os.path.join(plasma_dir, "socket")
        proc = subprocess.Popen(
            ["plasma_store", "-m", "100000", "-s", plasma_socket]
        )
        yield plasma_socket
        proc.terminate()
        proc.wait()
