
###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

0.2.1
*****

No functionality changes. The version number was bumped to 0.2.1 to 
match the version of the ska-sdp-dal image versions.

0.1.3
*****

- Fix error when attempting to put non-contiguous numpy arrays into the Plasma store

0.1.2
*****

- Introduce proper logging
- Allow querying number of used processors from caller
 
0.1.1
*****

- First published version, involving simply streaming and processor proof-of-concept
