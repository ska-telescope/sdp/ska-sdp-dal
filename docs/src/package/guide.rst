
*****************
API Documentation
*****************

The data access library is meant to enable the Science Data Processor
to exchange tensor and table data between processing functions at high
data rates. This is implemented using `Apache Arrow
<https://arrow.apache.org/docs/python/index.html>`_ data exchanged
over a `Apache Plasma
<https://arrow.apache.org/docs/python/plasma.html>`_ shared memory
object store.

====================
Processing Functions
====================


ska_sdp_dal.processor
---------------------

.. automodule:: ska_sdp_dal.processor

.. autoclass:: Processor
   :members:
   :undoc-members:

.. autoclass:: LogProcessor
   :members:
   :undoc-members:

.. autoclass:: BasicProcessor
   :members:
   :undoc-members:

   .. automethod:: _process_call

ska_sdp_dal.caller
---------------------

.. automodule:: ska_sdp_dal.caller
    :members:
    :undoc-members:

=======
Storage
=======
       
ska_sdp_dal.store
-----------------

.. automodule:: ska_sdp_dal.store
    :members:
    :undoc-members:

ska_sdp_dal.connection
----------------------

.. automodule:: ska_sdp_dal.connection
    :members:
    :undoc-members:

ska_sdp_dal.common
---------------------

.. automodule:: ska_sdp_dal.common
    :members:
    :undoc-members:

