import ska_sdp_dal as rpc
from ska_sdp_dal.common import _numpy_cast_to_complex, _numpy_cast_from_complex

import nifty_gridder as ng

import pyarrow
from pyarrow import plasma

import sys
from typing import Optional

NIFTY_PAR_SCHEMA = pyarrow.schema(
    [
        rpc.make_par("pixsize_x", pyarrow.float64()),
        rpc.make_par("pixsize_y", pyarrow.float64()),
        rpc.make_par("epsilon", pyarrow.float64()),
        rpc.make_par("do_wstacking", pyarrow.bool_()),
        rpc.make_par("nthreads", pyarrow.int64()),
        rpc.make_par("verbosity", pyarrow.int64()),
    ]
)

NIFTY_PROCS = [
    rpc.make_call_schema(
        "ms2dirty",
        [
            rpc.make_tensor_input_par("uvw", pyarrow.float64(), ["t", "uvw"]),
            rpc.make_tensor_input_par("freq", pyarrow.float64(), ["ch"]),
            rpc.make_tensor_input_par("ms", rpc.complex128, ["t", "ch"]),
            rpc.make_tensor_input_par(
                "wgt", pyarrow.float64(), ["t", "ch"], nullable=True
            ),
            rpc.make_par("npix_x", pyarrow.int64()),
            rpc.make_par("npix_y", pyarrow.int64()),
            rpc.make_table_input_par("pars", NIFTY_PAR_SCHEMA),
            rpc.make_tensor_output_par("output", pyarrow.float64(), ["x", "y"]),
        ],
    ),
    rpc.make_call_schema(
        "dirty2ms",
        [
            rpc.make_tensor_input_par("uvw", pyarrow.float64(), ["t", "uvw"]),
            rpc.make_tensor_input_par("freq", pyarrow.float64(), ["ch"]),
            rpc.make_tensor_input_par("dirty", pyarrow.float64(), ["x", "y"]),
            rpc.make_tensor_input_par(
                "wgt", pyarrow.float64(), ["t", "ch"], nullable=True
            ),
            rpc.make_table_input_par("pars", NIFTY_PAR_SCHEMA),
            rpc.make_tensor_output_par("output", rpc.complex128, ["t", "ch"]),
        ],
    ),
]


class NiftyCaller(rpc.Caller):
    def __init__(self, *args, **kwargs):
        super(NiftyCaller, self).__init__(NIFTY_PROCS, *args, **kwargs)


class NiftyProcessor(rpc.Processor):
    def __init__(self, *args, **kwargs):
        super(NiftyProcessor, self).__init__(NIFTY_PROCS, *args, **kwargs)

    def ms2dirty(
        self,
        uvw: rpc.TensorRef,
        freq: rpc.TensorRef,
        ms: rpc.TensorRef,
        wgt: Optional[rpc.TensorRef],
        npix_x: int,
        npix_y: int,
        pars: rpc.TableRef,
        output: rpc.TensorRef,
    ):

        # Get parameters from first row as dictionary
        pars_ = pars.get_awkward()[0].tolist()

        # Compute result
        output.put(
            ng.ms2dirty(
                uvw.get(),
                freq.get(),
                ms.get(),
                None if wgt is None else wgt.get(),
                npix_x,
                npix_y,
                **pars_
            )
        )

    def dirty2ms(
        self,
        uvw: rpc.TensorRef,
        freq: rpc.TensorRef,
        dirty: rpc.TensorRef,
        wgt: Optional[rpc.TensorRef],
        pars: rpc.TableRef,
        output: rpc.TensorRef,
    ):

        # Get parameters from first row as dictionary
        pars_ = pars.get_awkward()[0].tolist()

        # Compute result
        output.put(
            ng.dirty2ms(
                uvw.get(),
                freq.get(),
                dirty.get(),
                None if wgt is None else wgt.get(),
                **pars_
            )
        )


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("Please provide plasma socket path!")
        print("  Example: python processor.py /tmp/plasma")
        exit(1)

    # Create processor, process calls forever
    proc = NiftyProcessor(sys.argv[1])
    while True:
        try:
            proc.process()
        except KeyboardInterrupt:
            exit(0)
