
.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 3
  :caption: Home
  :hidden:


.. README =============================================================

.. This project most likely has it's own README. We include it here.

.. toctree::
   :maxdepth: 3

   ../../README.md
   package/usage
   package/guide


Project-name documentation HEADING
==================================

These are all the packages, functions and scripts that form part of the project.

- :doc:`package/guide`
