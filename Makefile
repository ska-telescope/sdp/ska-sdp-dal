
include .make/base.mk
include .make/python.mk
include .make/oci.mk

OCI_IMAGE=ska-sdp-dal-ci-arrow

# W503: line break before binary operator
PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=W503,E203
install-doc-requirements-with-poetry:
	poetry config virtualenvs.create $(POETRY_CONFIG_VIRTUALENVS_CREATE)
	poetry install --only main,docs

docs-pre-build: install-doc-requirements-with-poetry